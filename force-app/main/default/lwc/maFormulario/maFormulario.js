/**
 * @description       :
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             :
 * @last modified on  : 04-21-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
*/
import { LightningElement } from 'lwc'

export default class MaFormulario extends LightningElement {
    textValue;

    handleInputChange(event) {
        this.textValue = event.detail.value
    }
}