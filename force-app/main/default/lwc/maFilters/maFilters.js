/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
*/
import { LightningElement, api, wire } from 'lwc'
import { getPicklistValues } from 'lightning/uiObjectInfoApi'
import AREA_FIELD from '@salesforce/schema/Contact.Area__c'


export default class MaFilters extends LightningElement {

    @api label = ''

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: AREA_FIELD })
    areaValues;

    get picklistValues () {
        return this.areaValues?.data?.values.concat([{ label : 'All', value : 'All'}])
    }

    value = ''

    connectedCallback () {}

    handleChange (evt) {
        this.value = evt.detail.value
        // Cuando cambia el valor se lanza un evento ( CustomEvent, con dos parametros -> 1º nombre del evento, 2º info que quiero mandar en el evento ( un bojeto como parametro ))
        this.dispatchEvent(new CustomEvent('selectedfilter', { detail : { selectedArea: evt.detail.value }}))
    }
}