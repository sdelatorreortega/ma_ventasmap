/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
*/
import { LightningElement, track } from 'lwc'
import createTiendaApex from '@salesforce/apex/MA_GestionDeTiendasController.createTienda'

export default class MaCreateTienda extends LightningElement {

    // Para luego obtener los campos en un formato determinado =>
    // fields.map(f => {f.fieldApiName})
    // Output => {'Name', 'BillingLatitude', 'BillingLongitude'}
    // AL revés, tienes un formato y quieres pasarlo al formato de la variable 'fields' =>
    // let fieldApex = ['Name', 'BillingLatitude']
    //  let fields2 = fieldApex.map( f => ({ fieldApiName : f, value : ''}))
    // Output as Array=>  [{ fieldApiName : 'Name', value : ''}, { fieldApiName : 'BillingLatitude', value : ''}]

    @track account = {}

    @track fields = [
        { label : 'Name',  fieldApiName : 'Name', disabled : false, class : '' },
        { label : 'Latitude',  fieldApiName : 'BillingLatitude', disabled : true, class : '' },
        { label : 'Longitude',  fieldApiName : 'BillingLongitude', disabled : true, class : '' },
    ]

    @track createTiendaResult = ''

    get fieldsList () { return this.fields.map(f => (f.fieldApiName))}

    // get account () { return this.fields.map( f => ({ f.fieldApiName : f.value })) }

    setFieldValue (evt) {
        this.account[evt.currentTarget.dataset.field] = evt.detail.value
        const aux = this.fields.find( f => f.fieldApiName === evt.currentTarget.dataset.field)
        aux.value = evt.detail.value
        aux.class = evt.detail.value ? 'green-class' : 'red-class'
    }

    async createTiendaJs () {
        this.createTiendaResult = await createTiendaApex({ acc : this.account })
        console.log(JSON.stringify(this.createTiendaResult))

        // Refresca todos los componentes de la página
        eval("$A.get('e.force:refreshView').fire()")
    }
}