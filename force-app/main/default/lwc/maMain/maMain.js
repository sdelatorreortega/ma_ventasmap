/**
 * @description       :
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             :
 * @last modified on  : 04-22-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
*/

import { LightningElement, track } from 'lwc'

export default class MaMain extends LightningElement {

    @track selectedArea = ''

    setFilter (evt) {
        this.selectedArea = evt.detail.selectedArea
        console.log(this.selectedArea)
    }
}