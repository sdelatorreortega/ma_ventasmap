/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
*/
import { LightningElement, track } from 'lwc'
import getContacts from '@salesforce/apex/MA_EjerciciosLWCController.getContacts'
import getRankingBoardgame from '@salesforce/apex/MA_EjerciciosLWCController.getRankingBoardgame'
import getRankingBoardgameFront from '@salesforce/apex/MA_EjerciciosLWCController.getRankingBoardgameFront'
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class MaEjerciciosLWC extends LightningElement {

    title = 'Lista de Contactos';

    @track contacts = []

    @track rankingBoardgameBack = []

    @track rankingBoardgameFront = []

    showModal = false



    connectedCallback() {
        this.getAllContacts()
        this.getRankingBackend()
        this.getRankingFrontend()
    }

    // Obtiene el resultado desde una función de Apex y controla errores
    getAllContacts() {
        getContacts()
            .then(result => {
                this.contacts = result
            })
            .catch(err => {
                this.error = err
            })
    }

    // Obtiene el resultado desde una función de Apex y controla errores
    getRankingBackend() {
        getRankingBoardgame()
            .then(result => {
                this.rankingBoardgameBack = result
            })
            .catch(err => {
                this.error = err
            })
    }

    // Obtiene el resultado desde una función de Apex y controla errores
    getRankingFrontend() {
        getRankingBoardgameFront()
            .then(result => {
                this.rankingBoardgameFront = result
                // this.sortBoardgame(null, 'Nota__c')
            })
            .catch(err => {
                this.error = err
            })
    }

    //  Toast que muestra info sobre el primer juego de la tabla de Ranking de juegos (Backend)
    showToastBoardgame() {
        const toastEvent = new ShowToastEvent({
            title: "Info. del Primer Juego",
            message: 'El juego de mesa con el nombre ' + JSON.stringify(this.rankingBoardgameBack[0].Name) + ' tiene una  nota de => ' + JSON.stringify(this.rankingBoardgameBack[0].Nota__c),
            variant: "success"
        })
        this.dispatchEvent(toastEvent)
    }

    // Open the Boardgame´s Modal
    showModalBoardgame() {
        this.showModal = true
        this.modalTitle = 'Info. del Último Juego'
        this.modalInfo = 'El juego de mesa con el nombre ' + JSON.stringify(this.rankingBoardgameBack[1].Name) + ' tiene una  nota de => ' + JSON.stringify(this.rankingBoardgameBack[1].Nota__c)
    }

    // Close the Boardgame´s Modal
    hideModalBoardgame() {
        this.showModal = false
    }

    // defaultSortDirection = 'asc';

    // sortDirection = 'asc';

    // sortedBy;

    // Used to sort the 'Age' column
    // sortBy(field, reverse, primer) {
    //     const key = primer
    //         ? function (x) {
    //             return primer(x[field])
    //         }
    //         : function (x) {
    //             return x[field]
    //         }

    //     return function (a, b) {
    //         a = key(a)
    //         b = key(b)
    //         return reverse * ((a > b) - (b > a))
    //     }
    // }
}