/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
 * Error : No me renderiza el componente en la home page; tiendaId de la función setTienda aparece como undefined
*/
import { LightningElement, track, wire } from 'lwc'
import { getRecord } from 'lightning/uiRecordApi'
import { CurrentPageReference } from 'lightning/navigation'
import { registerListener, unregisterAllListeners } from 'c/pubsub'

const FIELDS = ['Account.Name']

export default class MaTiendaDetail extends LightningElement {

    @track tienda

    tiendaId

    @wire(getRecord, { recordId: '$tiendaId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {
            console.log(JSON.stringify(error))
        } else if (data) {
            this.tienda = data
        }
    }

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('locateTienda', this.setTienda, this)
    }

    disconnectedCallback() {
        unregisterAllListeners(this)
    }

    setTienda(payload){
        this.tiendaId = payload.tienda.Id
    }
}