/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
*/
import { LightningElement, track, wire } from 'lwc'
import { CurrentPageReference } from 'lightning/navigation'
import { registerListener, unregisterAllListeners } from 'c/pubsub'

export default class MaTiendasMap extends LightningElement {

    @track tienda

    @track zoomLevel = 15

    @track message = ''

    get tiendaToShow () { return [ { location : { Latitude : this.tienda?.BillingLatitude, Longitude : this.tienda?.BillingLongitude }}]}

    @wire(CurrentPageReference) pageRef

    connectedCallback() {
        //  El registerLinstener escucha los cambios que se puedan producir en la función locate Tienda, RegisterListener viene de pubsub.js file
        registerListener('locateTienda', this.locateTienda, this)
    }

    disconnectedCallback() {
        unregisterAllListeners(this)
    }

    // Func. que viene del pubsub.js file
    locateTienda(payload){
        this.tienda = payload.tienda
    }

}