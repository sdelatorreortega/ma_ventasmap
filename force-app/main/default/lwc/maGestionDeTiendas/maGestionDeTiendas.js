/* eslint-disable indent */
/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com
 */
import { LightningElement, track, wire } from 'lwc'
import getTiendasApex from '@salesforce/apex/MA_GestionDeTiendasController.getTiendasApex'
import { CurrentPageReference } from 'lightning/navigation'
import { fireEvent } from 'c/pubsub'

export default class MaGestionDeTiendas extends LightningElement {

  @track tiendas = [];

  // CurrentPageReference => viene de pubsub.js
  @wire(CurrentPageReference) pageRef

  connectedCallback() {
    this.getTiendas()
  }

  // Obtiene todas las tiendas desde la funcion detTiendasApex de MA_GestionDeTiendasController.cls (Apex)
  async getTiendas() {
    this.tiendas = await getTiendasApex()
  }

  handleClick(evt) {
  // fireEvent => viene de pubsub.js
    fireEvent(this.pageRef, 'locateTienda', { tienda : this.tiendas.find( t => t.Id === evt.currentTarget.dataset.tiendaid )})
    console.log('ENVIANDO EVENTO DE LOCATE TIENDA ' + evt.currentTarget.dataset.tiendaid)
  }

  // DEBUG
  // this.tiendas.find( t => {
  //       if (t.id === evt.currentTarget.dataset.tiendaid){

  //       return t
  //       }
  //       return undefined
  //   })

}