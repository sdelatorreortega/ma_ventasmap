/**
 * @description       :
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             :
 * @last modified on  : 04-21-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
*/
import { LightningElement } from 'lwc'

export default class MaButtonModalForm extends LightningElement {
    clickedButtonLabel;

    handleClick(event) {
        this.clickedButtonLabel = event.target.label
    }
}