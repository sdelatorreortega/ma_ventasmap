/**
 * @description  :
 * @author       : sdelatorre@omegacrmconsulting.com | [MM-DD-YYYY]
 */
import { LightningElement, api, track } from 'lwc'
import getRecordsCustom from '@salesforce/apex/MA_Controller.getRecordsCustom'

export default class MaRecordTable extends LightningElement {

    @api label = ''

    // option 2 = pinta la lista de que objeto: "contact"
    // y que campos consulta para pintar en la lista:  "name y city"
    // @api Object = 'Contact'
    // @api columns = ['Name', 'City']
    @api obj = ''

    @api name = ''

    @api city = ''

    @api area = 'Area__c'

    // Reflected Properties -> Cada vez que se cambie un valor desde el componente padre se llama al set para cambiar los valores de getRecords que se encuentran en el componente hijo, lanzando el metodo getRecords
    _selectedArea

    @api
    get selectedArea() {
        return this._selectedArea
    }

    set selectedArea(val) {
        this._selectedArea = val
        if(val) this.getRecords()
    }

    // option 1 = doy una lista y la pinta
    @track recordList = []

    get recordsSize () { return this.recordList?.length}

    get recordsEmpty () { return this.recordList?.length === 0}

    connectedCallback () {
        this.getRecords()
    }

    getRecords () {
        getRecordsCustom({  obj : this.obj, nameField : this.name, cityField : this.city, areaField : this.area, selectedArea: this.selectedArea })
            .then( res => {
                this.recordList = res
            })
            .catch( err => {
                console.error(err)
            })
    }
}