/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-04-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
trigger AccountAddressTrigger on Account (before insert, before update) {   

    for (Account a : Trigger.new) {
        if(a.Match_Billing_Address__c == true) {
            a.shippingPostalCode = a.BillingPostalCode;
            a.ShippingStreet = a.BillingStreet;
            a.ShippingCity = a.BillingCity;
        }
    }

}