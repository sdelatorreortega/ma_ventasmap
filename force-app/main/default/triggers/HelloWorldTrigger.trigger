/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-04-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
trigger HelloWorldTrigger on Account (before insert) {
    System.debug('Hello World!');
}