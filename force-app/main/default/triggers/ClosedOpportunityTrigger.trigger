/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-04-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {

    List<Task> taskList = new List<Task>();

    for (Opportunity opp : Trigger.New) {
        if(opp.StageName == 'Closed Won' ) {
            taskList.add(new Task(Subject = 'Follow Up Test Task', WhatId = opp.Id));
        }
    }
    if (taskList.size() > 0) {
        insert taskList;
    }
}