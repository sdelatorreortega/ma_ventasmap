/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 04-04-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
public with sharing class Academy { 
  
  public static Map<Id,Boardgame__c> returnBoardgameProducts() {
    List<Product2> product2Data = [SELECT Id, Name, Boardgame__c, (SELECT Id, Name FROM Juegos_de_mesa__r) FROM Product2];
    Map<Id,Boardgame__c> mapProduct2 = new Map<Id,Boardgame__c>();
    for(Product2 product : product2Data) {
      if(product.Boardgame__c !=null) {
        mapProduct2.put(product.Id, product.Juegos_de_mesa__r);
      }
    }
    return mapProduct2;
  }  

  public static Map<String, String> returnAvgCategoryProducts() {
    AggregateResult[] avgCategory = [SELECT Categoria__c, AVG(Nota__c)average FROM Boardgame__c GROUP BY Categoria__c];
    Map<String, String> mapCategoryAvg = new Map<String, String>();
    for (AggregateResult cat : avgCategory) {
      String category = String.ValueOf(cat.get('Categoria'));
      String note = String.ValueOf(cat.get('average'));
      mapCategoryAvg.put( category , Note );
    }
    return mapCategoryAvg;
  }

  public static void relationCustomerShop() {
    // Tiendas físicas relacionadas con clientes
    List<Account> physicShop = [SELECT Id, Name, (SELECT Id FROM Contacts) FROM Account WHERE RecordType.DeveloperName = 'Fisica'];

    // Almacenamos en una lista los clientes que no tienen una tienda habitual asignada.
    // además, en el WHERE metemos "AND UsuarioBGG__c != null AND Nivel__c != null" para que no salte el error "Required fields are missing"
    List<Contact> customerNoShop = [SELECT Id, Name FROM Contact WHERE Tienda_habitual__c = null AND UsuarioBGG__c != null AND Nivel__c != null];

    // Recorremos todos los clientes sin tienda, y para cada uno le asignamos la tienda con menos clientes
    for (Contact customer : customerNoShop) {
      System.debug(customer.Name);
      customer.Tienda_habitual__c = getShopWithLessClients(physicShop).ID;
    }

    // Finalmente, actualizamos la lista de clientes en la base de datos.
    update customerNoShop;
  }

  private static Account getShopWithLessClients(List<Account> shops) {
    Account shopWithLessCustomers = null;
    Integer numOfCustomers = 0;

    for(Account shop : shops){
      numOfCustomers = shop.Contacts.size();
      if (shopWithLessCustomers == null || numOfCustomers < shopWithLessCustomers.Contacts.size()){
        shopWithLessCustomers = shop;
      }
    }

    return shopWithLessCustomers;
  }
}