/**
 * @description  : 
 * @author       : sdelatorre@omegacrmconsulting.com
**/
public with sharing class MA_GestionDeTiendasController {
    @AuraEnabled
    public static List<Account> getTiendasApex() {
        List<Account> result = new List<Account>([SELECT Id, Name, BillingLongitude, BillingLatitude FROM Account WHERE  RecordType.Id ='0127Q000000DhFzQAK']);
        return result;
    }

    @AuraEnabled
    public static ResponseWrapper createTienda (Account acc) {
        insert acc;
        return new ResponseWrapper(acc.Id, false);
    }

    public class ResponseWrapper {
        public String message;
        public Boolean error;
        ResponseWrapper(String message, Boolean error) {
            this.message = message;
            this.error = error;
        }
    }
}