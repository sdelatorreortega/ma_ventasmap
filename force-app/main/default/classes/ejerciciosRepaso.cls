/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-12-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
public with sharing class ejerciciosRepaso {
    // Ejercicio 1: Construir dos conjuntos que contengan los ids de las cuentas con contactos relacionados. 
    // El primero de ellos haciendo query sobre Contact y el segundo sobre Account.
    public static void ejercicio1 () {
        List<Contact> idContactCuenta = [SELECT Id, Name, Estudio__c FROM Contact];
        List<Account> idAccountCuenta = [SELECT Id, Name, (SELECT Id, Name FROM Alumnos__r) FROM Account];
    }


    // Ejercicio 2: Construir dos mapas que tenga como key el Id de una cuenta y como valor la lista de contactos relacionados. 
    // El primero de ellos haciendo query sobre Contact y el segundo sobre Account.
    public static void ejercicio2 () { 

        /**  Query sobre Account */
        // List<Contact> contactsRelated = [SELECT Id, Name FROM Contact];
        // Key --> Id de cuenta
        List<Account> idAccountKey = [SELECT Id, Name FROM Account WHERE Account.RecordType.DeveloperName = 'Estudios'];
        List<Account> idAccountRelated = [SELECT Id, Name, (SELECT Id, Name FROM Alumnos__r) FROM Account];
        Map<Id, List<Contact>> accountMap = new Map<Id, List<Contact>>();
        
        for(Account acct : idAccountRelated ) {
            if(acct.Alumnos__r.size() > 0) {
                accountMap.put(acct.Id, acct.Alumnos__r);
            }   
        }
        System.debug(accountMap);

        // List<Account> idAccountKey = [SELECT Id, Name FROM Account WHERE Account.RecordType.DeveloperName = 'Estudios'];
        // List<Account> idAccountRelated = [SELECT Id, Name, (SELECT Id, Name FROM Alumnos__r) FROM Account];
        // Map<Id, List<Contact>> accountMap = new Map<Id, List<Contact>>();
        
        // for(Account acct : idAccountRelated ) {
        //     // Value --> Lista contactos relacionados
        //     List<Contact> newAcct = new List<Contact>();
        //     Boolean haveContact = false;

        //     for(Contact contact : acct.Alumnos__r) { 
        //         if (contact != Null) {
        //             newAcct.add(contact);
        //             haveContact = true;
        //         }
        //     }    
        //     if(haveContact) {
        //         accountMap.put(acct.Id, newAcct);
                
        //     }   
        // }
        // System.debug(accountMap);
    }

    // Ejercicio 3: Construir un mapa cuya key sea el id del contacto y value y la lista de solicitudes relacionadas.
    public static void ejercicio3 () {
        // key --> Id del contacto
        List<Contact> idContactRelated = [SELECT Id, Name, (SELECT Id, Name FROM Admisiones__r) FROM Contact];
        Map<Id, List<Opportunity>> idContactRelatedMap = new Map<Id, List<Opportunity>>();

        for(Contact contact : idContactRelated) {
            // Value --> Lista Admisiones relacionadas (contact.Admisiones__r)
          
            if(contact.Admisiones__r.size() > 0) {
                idcontactRelatedMap.put(contact.Id, contact.Admisiones__r); 
             
            }   
        }
        System.debug('FINAL = ' + idcontactRelatedMap.values());   
        // List<Contact> idContactRelated = [SELECT Id, Name, (SELECT Id, Name FROM Admisiones__r) FROM Contact];
        // Map<Id, List<Opportunity>> idContactRelatedMap = new Map<Id, List<Opportunity>>();
        // Boolean haveOpp = false;

        // for(Contact contact : idContactRelated) {
        //     // Value --> Lista Admisiones relacionadas (contact.Admisiones__r)
        //     List<Opportunity> newOpps = new List<Opportunity>();
            
        //     for(Opportunity opp : contact.Admisiones__r) {             
        //         if(opp != Null) {
        //             newOpps.add(opp);
        //             haveOpp = true;
        //         }
        //     }
        //     if(haveOpp) {
        //         idcontactRelatedMap.put(contact.Id, newOpps); 
        //         haveOpp = false;
        //     }   
        // }
        // System.debug('FINAL = ' + idcontactRelatedMap.values());   
    }

    // Ejercicio 4: Construir un mapa de String a lista de Maestros de documentación. 
    // La key del mapa será el código del estudio 
    // y la lista contendrá el registro de Maestro_Documentación con su identificador y su nombre.
    public static void ejercicio4 () {
        
        Map<String, List<String>> maestroDocumentacionMap = new Map<String, List<String>>();
        // Value map
        List<MaestroDocumentacion__c> registroMaestro = new List<MaestroDocumentacion__c>([SELECT Id, Name FROM MaestroDocumentacion__c]);
        // Key map
        List<Account> codigoEstudio = new List<Account>([SELECT Codigo__c FROM Account]);
        
        for(Integer i = 0; i < registroMaestro.size(); i++) {
            
            for(Account code : codigoEstudio ) {
                // Value map
                List<String> concatenated = new List<String>();           
                concatenated.add(code.Id + code.Name);
                maestroDocumentacionMap.put(code.Codigo__c, concatenated);
            }
        }        
        System.debug('DEBuG 4:  ' + maestroDocumentacionMap);    
        // Map<String, List<String>> maestroDocumentacionMap = new Map<String, List<String>>();
        // // Value map
        // List<MaestroDocumentacion__c> registroMaestro = new List<MaestroDocumentacion__c>([SELECT Id, Name FROM MaestroDocumentacion__c]);
        // List<Account> codigoEstudio = new List<Account>([SELECT Id, Name, Codigo__c FROM Account]);
        
        // for(Integer i = 0; i < registroMaestro.size(); i++) {
            
        //     for(Account code : codigoEstudio ) {
        //         // Value map
        //         List<String> concatenated = new List<String>();           
        //         concatenated.add(code.Id + code.Name);
        //         maestroDocumentacionMap.put(code.Codigo__c, concatenated);
        //     }
        // }        
        // System.debug('DEBuG 4:  ' + maestroDocumentacionMap);   
    }
}