/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-22-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
*/
public with sharing class MA_Controller {

    @AuraEnabled
    public static List<RecordWrapper> getRecordsCustom(String obj, String nameField, String cityField, String areaField, String selectedArea) {
       
        List<RecordWrapper> recordsList = new List<RecordWrapper>();
        
        String query = 'SELECT Id,' + nameField + ', ' + cityField + ', ' + areaField + ' FROM ' + obj;
        
        if(String.isNotblank(selectedArea) && selectedArea != 'All') {
            query += ' WHERE Area__c = :selectedArea';
        }

        for (sObject record : Database.query(query)) {
            recordsList.add(new RecordWrapper(record.Id,(String)record.get(nameField),(String)record.get(cityField),(String)record.get(areaField)));     
        }
    
        return recordsList;
    }

    public class RecordWrapper {
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public String city;
        @AuraEnabled public String area;

        public RecordWrapper(String id, String name, String city, String area) {
            this.id = id;
            this.name = name;
            this.city = city;
            this.area = area;
        }
    }
}