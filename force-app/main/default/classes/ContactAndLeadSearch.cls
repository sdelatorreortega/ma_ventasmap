/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-30-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads (String incomingParameter) {
        List<List<SObject>> searchList = 
        [FIND :incomingParameter IN ALL FIELDS RETURNING Lead(FirstName,LastName), Contact(FirstName,LastName)];
        return searchList;
    }
}