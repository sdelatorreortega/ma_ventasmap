/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-03-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
public with sharing class AccountUtility {
    public static void viewAnnualRevenue() {
        list<Account> accountsList = [SELECT Name, AnnualRevenue FROM Account];
        for (Account acct : accountsList) {
            String acctRev = acct.Name + ' : ' + acct.AnnualRevenue;
            system.debug(acctRev);
        }
    }
}