/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-09-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/

public with sharing class ContactTriggerHandler {
    // *Realizar sin tirar ninguna query directamente a Contact
    // Desarrollar un desencadenador (trigger) que inserte una nueva tarea (Task) por cada contacto que se actualice. La tarea será de tipo (Type) llamada y estará relacionada con el contacto a través del campo WhoId.
    
    // Mejora 1 - La tarea creada deberá tener un campo personalizado (AccountName__c) que contenga el nombre de la cuenta asociada con el contacto. Para evitar usar una query dentro del bucle, utilizar un Map<Id,Account>.
    
    // Mejora 2 - Solo deberá crearse la tarea cuando el campo Phone del contacto se haya modificado.
    
    public static void insertTasks() {
        List<Task> taskList = new List<Task>();
        //Mapa Id contact --> account
        //List<Account> accList = [SELECT Id, Name FROM Account WHERE Id In (SELECT Tienda_habitual__c FROM Contact WHERE Id IN : Trigger.new)];
        
        Map<Id,Account> contAcctMap = new Map<Id,Account>();

        for(Contact contact : (List<Contact>) Trigger.new) {
            if(contact.Phone != ((Map<Id, contact>) Trigger.oldMap).get(contact.Id).Phone) {
                taskList.add(new Task(
                    WhoId = contact.Id,
                    Type = 'Call', 
                    AccountName__r = contAcctMap.get(contact.Id) 
                ));
            }
        }    
        insert taskList;
    }

    public static void setTiendaHabitual() {
        set<Id> tiendasId = new Set<Id>();
        set<String> ciudades = new Set<String>();

        for(Contact contact : [SELECT Id, MailingCity, Tienda_habitual__c FROM Contact WHERE MailingCity != null]) {
            tiendasId.add(contact.Tienda_habitual__c);
            ciudades.add(contact.MailingCity);
        }
        system.debug(tiendasId);
        system.debug(ciudades);
    }
    

}
