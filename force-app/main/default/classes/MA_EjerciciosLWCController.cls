/**
 * @description  : 
 * @author       : sdelatorre@omegacrmconsulting.com
*/
public with sharing class MA_EjerciciosLWCController {
    
    // Obtiene todos los Contactos
    @AuraEnabled
    public static List<Contact> getContacts() {
        return [SELECT Id, Name FROM Contact  LIMIT 5];
    }

    // Hace un ranking de los Juegos de mesa 
    @AuraEnabled
    public static List<Boardgame__c> getRankingBoardgame() {
        return [SELECT Id, Name, Nota__c FROM Boardgame__c ORDER BY Nota__c DESC LIMIT 5];
    }

    @AuraEnabled
    public static List<Boardgame__c> getRankingBoardgameFront() {
        return [SELECT Id, Name, Nota__c FROM Boardgame__c];
    }
}