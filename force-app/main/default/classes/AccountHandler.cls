/**
 * @description       : 
 * @author            : sdelatorre@omegacrmconsulting.com - with SFDoc
 * @group             : 
 * @last modified on  : 04-03-2022
 * @last modified by  : sdelatorre@omegacrmconsulting.com - with SFDoc
**/
public with sharing class AccountHandler {
    public static void insertAccount (Integer value) {
        List<Account> addAccounts = new List<Account>();
        Integer counter = 1;
        while (counter <= value) {
            Account n = new Account();
            n.Name = 'Acme Inc N';
            n.AccountNumber = 'A000n';
            addAccounts.add(n);
            counter = counter + 1;
        }
        insert addAccounts;
    }
}
